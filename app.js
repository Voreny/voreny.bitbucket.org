(function() {
    var app = angular.module("showOff", []);

    app.controller("ProjectsCtrl", function ($scope, $http){
        var projects = [], mainProject = {};


        $http.get("project-data.json").success(function (data)
        {
            $scope.mainProject = data.projects[0];
            $scope.projects = data.projects.slice(1);
        });
    });
})();